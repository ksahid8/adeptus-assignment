import "./App.css";
import MyLayout from "./Layout/Layout";
import Dashboard from "./Pages/Dashboard";

function App() {
  return (
    <div className="App">
      <MyLayout>
        <Dashboard />
      </MyLayout>
    </div>
  );
}

export default App;
