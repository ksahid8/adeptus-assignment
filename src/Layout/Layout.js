import React, { useState } from "react";
import { Layout, Menu } from "antd";
import {
  MenuUnfoldOutlined,
  MenuFoldOutlined,
  DashboardOutlined,
  ShoppingOutlined,
  ShoppingCartOutlined,
  UserOutlined,
  FileDoneOutlined,
  ShopOutlined,
} from "@ant-design/icons";

import logo from "./../Images/amazon_logo.png";
import icon from "./../Images/amazon_icon.png";

const MyLayout = (props) => {
  const { Header, Sider, Content } = Layout;

  const [Collapsed, setCollapsed] = useState(false);

  const toggle = () => {
    setCollapsed(!Collapsed);
  };

  return (
    <Layout>
      <Sider trigger={null} collapsible collapsed={Collapsed}>
        <div className="logo">
          <img className="full-width" src={Collapsed ? icon : logo} />
        </div>
        <Menu theme="dark" mode="inline" defaultSelectedKeys={["1"]}>
          <Menu.Item key="1" icon={<DashboardOutlined />}>
            Dashboard
          </Menu.Item>
          <Menu.Item key="2" icon={<ShoppingOutlined />}>
            Sales
          </Menu.Item>
          <Menu.Item key="3" icon={<ShoppingCartOutlined />}>
            Orders
          </Menu.Item>
          <Menu.Item key="4" icon={<FileDoneOutlined />}>
            Invoices
          </Menu.Item>
          <Menu.Item key="5" icon={<ShopOutlined />}>
            Shipments
          </Menu.Item>
          <Menu.Item key="6" icon={<UserOutlined />}>
            Users
          </Menu.Item>
        </Menu>
      </Sider>
      <Layout className="site-layout">
        <Header className="site-layout-background" style={{ padding: 0 }}>
          {React.createElement(
            Collapsed ? MenuUnfoldOutlined : MenuFoldOutlined,
            {
              className: "trigger",
              onClick: () => toggle(),
            }
          )}
        </Header>
        <Content
          className="site-layout-background"
          style={{
            margin: "24px 16px",
            padding: 24,
            minHeight: 280,
          }}
        >
          {props.children}
        </Content>
      </Layout>
    </Layout>
  );
};
export default MyLayout;
