import React from "react";
import GridLayout from "react-grid-layout";
import BarChart from "../Components/BarChart";
import LineChart from "../Components/LineChart";
import GridOptions from "./../Data/GridOptions.json";
const ChartContainer = () => {
  const LayoutOption = GridOptions.chartcontainer;
  return (
    <GridLayout
      className="layout"
      layout={LayoutOption}
      isResizable={false}
      cols={8}
      width={1000}
      rowHeight={100}
      margin={[20, 10]}
    >
      <div key="x">
        <div className="chart-card">
          <LineChart />
        </div>
      </div>
     <div key="y">
        <div className="chart-card">
          <BarChart />
        </div>
      </div> 
    </GridLayout>
  );
};
export default ChartContainer;
