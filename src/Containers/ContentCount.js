import React from "react";
import GridLayout from "react-grid-layout";
import CountCard from "../Components/CountCard";
import {
  ShoppingOutlined,
  ShoppingCartOutlined,
  FileDoneOutlined,
  ShopOutlined,
} from "@ant-design/icons";

import GridOptions from "./../Data/GridOptions.json";

const ContentCount = () => {
  const LayoutOption = GridOptions.contentcount;

  return (
    <GridLayout
      className="layout"
      layout={LayoutOption}
      isResizable={false}
      cols={8}
      width={1000}
      rowHeight={100}
      margin={[20, 20]}
    >
      <div key="a">
        <CountCard title={"Total Sales"} icon={ShoppingOutlined} count={26} />
      </div>
      <div key="b">
        <CountCard
          title={"Total Orders"}
          icon={ShoppingCartOutlined}
          count={47}
        />
      </div>
      <div key="c">
        <CountCard
          title={"Total Invoices"}
          icon={FileDoneOutlined}
          count={23}
        />
      </div>
      <div key="d">
        <CountCard title={"Total Shipments"} icon={ShopOutlined} count={31} />
      </div>
    </GridLayout>
  );
};
export default ContentCount;
