import React from "react";

import GridLayout from "react-grid-layout";
import OrderTable from "../Components/OrderTable";
import GridOptions from "./../Data/GridOptions.json";
const TableContainer = () => {
  const LayoutOption = GridOptions.tablecontainer;
  return (
    <GridLayout
      className="layout"
      layout={LayoutOption}
      isResizable={false}
      cols={8}
      width={1000}
      rowHeight={100}
      margin={[20, 10]}
    >
      <div key="x">
          <OrderTable />
      </div>
    </GridLayout>
  );
};
export default TableContainer;
