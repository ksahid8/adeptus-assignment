import React from "react";
import "react-grid-layout/css/styles.css";
import "react-resizable/css/styles.css";
import ChartContainer from "../Containers/ChartContainer";
import ContentCount from "../Containers/ContentCount";
import TableContainer from "../Containers/TableContainer";

const Dashboard = () => {
  return (
    <>
      <ContentCount />
      <ChartContainer />
      <TableContainer />
    </>
  );
};
export default Dashboard;
