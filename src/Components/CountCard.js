import React from "react";
import Icon from "@ant-design/icons";

const CountCard = (props) => {
  return (
    <div className="count-card">
      <Icon component={props.icon} />
      <div className="count-number">
        <h3>{props.count}</h3>
      </div>
      <div className="count-title">
        <h6>{props.title}</h6>
      </div>
    </div>
  );
};
export default CountCard;
