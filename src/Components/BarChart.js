import React from "react";
import { Bar } from "react-chartjs-2";

const data = {
  labels: ["1", "2", "3", "4", "5", "6"],
  datasets: [
    {
      label: "Online",
      data: [12, 19, 3, 5, 2, 3],
      backgroundColor: "#ec7211",
    },
    {
      label: "COD",
      data: [2, 3, 20, 5, 1, 4],
      backgroundColor: "#ffac56",
    },
    {
      label: "Gift Wrap",
      data: [3, 10, 13, 15, 22, 30],
      backgroundColor: "#ffd356",
    },
  ],
};

const options = {
  scales: {
    yAxes: [
      {
        stacked: true,
        ticks: {
          beginAtZero: true,
          stepSize: 10,
        },
      },
    ],
    xAxes: [
      {
        stacked: true,
      },
    ],
  },
  legend: {
    position: "bottom",
  },
};

const BarChart = () => (
  <>
    <div className="header">
      <h1 className="title marginbottom60">Stacked Bar Chart</h1>
    </div>
    <Bar data={data} options={options} />
  </>
);

export default BarChart;
