import React from "react";
import Highcharts from "highcharts";
import HighchartsReact from "highcharts-react-official";
import ChartData from "./../Data/ChartData.json";

const ChartSeries = ChartData.lineseries;
const options = {
  title: {
    text: "Line chart",
  },

  series: ChartSeries,
  colors: ["#ec7211", "#ffac56", "#ffd356"],
};

const LineChart = () => (
  <>
    <HighchartsReact highcharts={Highcharts} options={options} />
  </>
);

export default LineChart;
